## Connect to Anki

Game2Text connects to Anki through AnkiConnect. To install that, copy the addon code on [AnkiConnect's website](https://ankiweb.net/shared/info/2055492159) and add it to your Anki program.

!!! Info
    The AnkiConnect addon is required for Game2Text to connect to Anki.

With Anki running, in Game2Text click :material-palette-swatch: **Anki**.

<img src="../../assets/anki-tooltip.png" alt="anki-tooltip" width="500">

## Anki Setup

Select the **Deck** for the cards to be added to and their card **Model**. If you don't see your decks or models, click **Reload** at the bottom. 

When you select your card model, your model table will be shown and you can select the corresponding values for each field.

<img src="../../assets/anki-settings-map.png" alt="anki-settings" width="500">

Here is what each value does.

Name | Description |
--- | --- |
sentence | text extracted from the game |
selected word | words you highlighted |
reading | reading of the selected word if it's a dictionary entry |
glossary | definition of the selected word if it's a dictionary entry |
audio | recorded audio of the game |
screenshot | screenshot of the game when the text was extracted |

## Create an Anki Card

Click :material-view-list: **Logs** to open the logs window. 

<img src="../../assets/logs-tooltip.png" alt="logs-tooltip" width="500">

Hover over a log to view its Anki card.

<img src="../../assets/anki-card-preview.png" alt="logs-tooltip" width="500">

**Highlight a phrase to select it**. Nouns are directly added, while conjugated verbs and adjectives will be replaced by its base form. 

If the phrase is a dictionary entry, other fields such as glossary, reading (furigana), and word audio will be added.

<img src="../../assets/anki-highlight-words.png" alt="anki-tooltip" width="500">

Click **Save to Anki** or the keyboard shortcut **Shift-E** to add this card.

!!! Important
    You can also add cards to anki by highlighting words in the main window. This makes adding cards to Anki super fast by **holding shift to show words through Yomichan and pressing E to immediately add them.**