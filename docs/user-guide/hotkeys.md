Hotkeys can greatly improve your productivity. Use them to refresh OCR or **add cards to Anki while you're playing the game!**

## Default Hotkeys 

Action | Windows | MacOS | Linux | 
--- | --- | --- | --- |
Refresh OCR | Ctrl-Q | Cmd-B | Ctrl-Q |
Add to Anki | Shift-E | Shift-E | Shift-E |
Record Audio | Ctrl-L | Cmd-L | Ctrl-L |

## Change Hotkeys

To change hotkeys, click :material-cog: **Settings** and select the **Hotkeys** tab. 

The format for action keys is `<action>` as in `<ctrl>` and `<shift>` and you can combine keys with `+`. 

For example, on Windows, *Control-B* is `<ctrl>+b` and *Shift-Alt-P* is `<shift>+<alt>+p`. 

On Mac, *Command-B* is `<cmd>+b`.

!!! Info
    Hotkey changes only take effect upon restarting Game2Text.

<img src="../../assets/hotkeys.png" alt="hotkeys" width="500">
