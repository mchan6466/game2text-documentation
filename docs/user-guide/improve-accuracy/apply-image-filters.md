Text recognition may not work well with light or noisy backgrounds. This is where image profiles come in handy.

## Change Image Filters

To apply image filters, **right click on the game stream**.

The selection image is shown on the top part of the image filter window. 

<img src="../../../assets/apply-image-filters-popup.png" alt="image-filters-settings" width="500">

!!! Failure
    There are lots of background noise in this text selection, and because of that the OCR result is poor.

## Binarize

Click on the **Binarize** checkbox and see how that affects the selection image.

<img src="../../../assets/apply-image-filters-binarize.png" alt="image-filters-binarize" width="500">

!!! Success
    The background noise is gone and now the text is successfuly recognized. 

There are other image settings here but the idea is to remove as much background noise as possible and increase the contrast between the text and the background.

## Import and Export Image Filter Profiles

Click **Export** to save an image filter preset for later use or for sharing with others. The **light-background** preset is an included preset that works generally well for games with light backgrounds. 

<img src="../../../assets/apply-image-filters-select.png" alt="image-filters-binarize" width="500">
