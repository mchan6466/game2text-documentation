## Switch OCR Engine

Click on :material-cog: **Settings**. On the **General** tab click to select the following OCR modes.

<img src="../../../assets/ocr-settings.png" alt="ocr-settings" width="500">

In short, Tesseract runs locally which will be faster on most modern machines, while OCR Space, being an online service, is slower but sometimes provides better results.

## OCR Mode Differences

Here are more detailed differences between each OCR mode.

OCR Mode | Engine | Provider | Framework | License |
--- | --- | --- | --- | --- |
Tesseract Default | Tesseract 4.1.1 | Local | CRNN | Open Source |
Tesseract LSTM | Tesseract 4.1.1 | Local | RNN | Open Source |
Tesseract Legacy | Tesseract 3 | Local | CNN | Open Source |
OCR Space NA | Computer Vision Read API | Azure Cloud | R-CNN | Proprietary (Microsoft) |
OCR Space EU | Computer Vision Read API | Azure Cloud | R-CNN | Proprietary (Microsoft) |

