By default, Game2Text is only available on secondary screens when you set your game to full screen.

**Using external applications, Game2Text can float on the same screen as your game.**

<img src="../../assets/full-screen-deskpins.png" alt="full-screen-gaming" width="800">

An additional benefit is that the Anki screenshots taken have no window borders. 

!!! Info
    The following tools are available only on Windows.

## Use Borderless Gaming to Go Full Screen

[:material-download: Download Borderless Gaming](https://github.com/Codeusa/Borderless-Gaming/releases){ .md-button}

Add your game application to the favorites list in Borderless Gaming. 

<img src="../../assets/full-screen-borderless-gaming.png" alt="borderless-gaming" width="500">

Next, run your game in **windowed mode**.

Whenever Bordleress Gaming is active, it will convert your application into a custom full screen mode that gives you control over your cursor and taskbar. 

## Pin Game2Text windows to the game with DeskPins

[:material-download: Download DeskPins](https://efotinis.neocities.org/deskpins/){ .md-button}

**Add a pin** by clicking on the DeskPin icon hidden in the taskbar.

<img src="../../assets/full-screen-deskpins-taskbar.png" alt="deskpins">

Pin the main window and the log window of Game2Text.

**A red pin should be visible on them.**

<img src="../../assets/full-screen-anki.png" alt="full-screen-gaming" width="800">

They will now stick around while you play the game.

If everything works, **you should be able to hover over words without having to click the windows.**