## Enable Auto Mode

Auto-mode allows you to keep playing the game while Game2Text looks for the next line of text in the game. To enable auto mode, **click on :material-autorenew: Auto Mode**.

<img src="../../assets/auto-tooltip.png" alt="auto-tooltip" width="500">

Once auto-mode is enabled, Game2Text will try to scan the game for text every time the encircled part changes. **To stop auto-mode click on :material-autorenew: Auto Mode again.**

<img src="../../assets/auto-enabled.png" alt="auto-enabled" width="500">

