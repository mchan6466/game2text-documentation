## Show Translation

To show translation, click on the :material-translate: **Translate** icon.

<img src="../../assets/translate-tooltip.png" alt="translate-tooltip" width="500">

Once translation is enabled, translation is updated every time a new line of text is extracted.

## Translation Settings

<img src="../../assets/translate-enabled.png" alt="translate-enabled" width="500">

To change the translation service or your target language, click :material-cog: **Settings** and switch to the **Translation** tab.

<img src="../../assets/translate-settings.png" alt="translate-settings" width="500">

Currently there are three available translation services - Papago, DeepL, and Google. They are all online services and may have limitations. As a result, you may have to alternate between them. 

!!! warning
    Translation services like DeepL may be limited for a single IP address.