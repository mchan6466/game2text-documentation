When the visual novel hooker in Game2Text does not work, you can try clipboard mode instead.

Example apps include other text hookers like Textractor or custom hookers like the [Steins;Gate hooker](https://github.com/shiiion/steinsgate_textractor).

## Enable Clipboard to Game2Text

Click :material-cog: **Settings** and select the **Clipboard** tab.

<img src="../../../assets/clipboard-mode-settings.png" alt="clipboard-mode-settings" width="500">

Click the **Clipboard-inserter** switch. 

<img src="../../../assets/clipboard-mode-enabled.png" alt="clipboard-mode-enabled" width="500">


