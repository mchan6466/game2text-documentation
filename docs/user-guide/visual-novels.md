Visual Novels, often abbreviated as VN, are interactive fiction video games, featuring text-based story with narrative style of literature and interactivity aided by static or sprite-based visuals. Game2Text includes a **text hooker application** to extract text from visual novels. 

!!! info
    Visual Novel hooking is available only on Windows.

## Select application

In the toolbar, click :material-book-open-variant: **Visual Novel Hooker**.

![image](https://user-images.githubusercontent.com/13146030/117126192-01800480-adcd-11eb-8fb6-1f10dd19dde1.png)

Type and select your application.

<img src="https://user-images.githubusercontent.com/13146030/117129608-7bb28800-add1-11eb-861e-f3da671afccc.png" width="500">

If you're not sure what your application's process name is, launch Window's **Task Manager** and look for your game.


<img src="https://user-images.githubusercontent.com/13146030/117133558-fcc04e00-add6-11eb-95c5-480e49a9932b.png" width="400">

## Select hook

Continue with your game until text appears. 

Successfully found hooks will be shown and you can select them.

![image](https://user-images.githubusercontent.com/13146030/117134187-d818a600-add7-11eb-8218-c69622bbf92f.png)

Now that the the game is hooked, text will be automatically extracted without needing to draw on the game for image recognition.





## Troubleshooting: missing dialog
Sometimes the game will have multiple hooks and you may need to change the hooks for certain dialog.

## Troubleshooting: architecture mismatch

If the Textractor logs says **architecture mismatch**, it means that this game requires a 64 bit version Texthooker. Download the latest 64 bit version of [Textractor](https://github.com/Artikash/Textractor) and replace the files **TextractorCLI** and **texthook.dll** in *game2text/resources/bin/win/textractor/*.