The easiest way to extract text through games is through image recognition.

## Extract text with image recognition

Click on **Select Application**.

<img src="../../assets/quick-start-app-welcome.png" alt="app-welcome" width="500">

Select your game or application window and click **Share**.

<img src="../../assets/quick-start-select-app.png" alt="yomichan-demo" width="500">

Click and drag over the game screen to create a selection over a text region. The text will be extracted.

<img src="../../assets/quick-start-draw-demo.png" alt="draw-demo" width="500">

## Look up vocabulary with popup dictionaries

Popup dictionaries like [Yomichan](https://chrome.google.com/webstore/detail/yomichan/ogmnaimimemjmbakcfefmnahgdfhfami) and [Rikaichan](https://chrome.google.com/webstore/detail/rikaichan/clidkjbfdlffpbbhlalnkifiehenkjaj) are immediately accessible.

Hover over a word or kanji and hold **shift** to bring up the dictionary.

<img src="../../assets/quick-start-yomichan-demo.png" alt="yomichan-demo" width="500">