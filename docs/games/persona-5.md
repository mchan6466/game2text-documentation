![image](../../assets/persona5.jpg)

## Language Diffculty - Medium

Game | Subtitles | Voiced | Furigana | Platform | Game Font
--- | --- | --- | --- | --- |
Persona 5 | JP/EN | Scenes Only | ❌ | PS4 | [スランプ](https://en.fontworks.co.jp/fontsearch/slumpstd-db/) |
Persona 5 Royal | JP/EN | Scenes Only | ❌ | PS4, PS5 | [スランプ](https://en.fontworks.co.jp/fontsearch/slumpstd-db/) |

!!! Note
    The game version determines the subtitle language. You must buy the Japanese version for Japanese subtitles.

## OCR Accuracy - Low
Text recognition for Persona 5 is not that great with default Tesseract models. You might get better results with models trained with the [スランプ font](https://en.fontworks.co.jp/fontsearch/slumpstd-db/).

That being said, there's a partial game script for Persona 5.

## Persona 5 Game Script

The Persona 5 game script can help you improve your OCR result by game script matching. [Learn more about game script matching with Game2Text.](/user-guide/improve-accuracy/import-game-scripts/)

[:material-download: Download Script](https://github.com/mathewthe2/Game2Text-GameScripts/blob/main/gamescripts/Persona%205.txt){ .md-button .md-button--primary }

!!! Warning
    A significant portion of game dialog is not included in this game script
