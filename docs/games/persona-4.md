![image](../../assets/persona4.jpg)

## Language Diffculty - Medium

Person 4 and Persona 4 Golden is an excellent start for language learners with around N4-N3 level to start immersing with the help of OCR tools and a dictionary.

Game | Subtitles | Voiced | Furigana | Platform | Game Font
--- | --- | --- | --- | --- |
Persona 4 | JP/EN | Scenes Only | ❌ | PS2 | [スキップ](https://fontworks.co.jp/fontsearch/skipstd-b/) |
Persona 4 Golden | JP/EN | Scenes Only | ❌ | PC, PS3, PS Vita | [スキップ](https://fontworks.co.jp/fontsearch/skipstd-b/) |

## OCR Accuracy - High

Text recognition for Persona 4 is pretty good with Tesseract 4 without any image filtering presets. 

With [game script matching](/user-guide/improve-accuracy/import-game-scripts/) in Game2Text, you can achieve almost perfect accuracy.

## Persona 4 Game Script

The Persona 4 game script can help you improve your OCR result by game script matching. [Learn more about game script matching with Game2Text.](/user-guide/improve-accuracy/import-game-scripts/)

[:material-download: Download Script](https://github.com/mathewthe2/Game2Text-GameScripts/blob/main/gamescripts/Persona%204.txt){ .md-button .md-button--primary }
