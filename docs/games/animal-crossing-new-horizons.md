![image](../../assets/acnh.jpg)

## Language Difficulty - Beginner to Medium

Animal Crossing lets you build your island and interact with your islanders at your own pace. It uses simple language and covers a lot of words of natural wildlife. The biggest drawback for language learners is the lack of Japanese voice. 

Game | Subtitles | Voiced | Furigana | Platform | Game Font
--- | --- | --- | --- | --- |
Animal Crossing: New Horizons | JP/EN | ❌ | ✅ | Nintendo Switch | [ロダン墨東 UB](https://fontworks.co.jp/fontsearch/rodinwanpakupro-db/) & [スーラ（N仕様）](https://fontworks.co.jp/fontsearch/seuratpron-db/) |

## Animal Crossing: New Horizons Game Script

The Animal Crossing game script can help you improve your OCR result by game script matching. [Learn more about game script matching with Game2Text.](/user-guide/improve-accuracy/import-game-scripts/)

[:material-download: Download Script](https://github.com/mathewthe2/Game2Text-GameScripts/blob/main/gamescripts/Animal%20Crossing%20New%20Horizons.txt){ .md-button .md-button--primary }