# Download

Game2Text is 100% free. 

If you'd like to support me, you can pay-what-you-want on [Gumroad](https://gum.co/game2text).

## :material-microsoft-windows: Windows

[:material-download: Game2Text for Windows](https://github.com/mathewthe2/Game2Text/releases){ .md-button }

After unzipping the file, move the folder to your desktop or a destination in which you have **write permission**.

!!! information
    If Game2Text fails to launch, try installing [Microsoft's C++ build tools](https://visualstudio.microsoft.com/visual-cpp-build-tools/).

## :material-apple: MacOS

[:material-download: Game2Text for Mac](https://github.com/mathewthe2/Game2Text/releases){ .md-button }

1. After unzipping the file, right click and launch the app or drag it to your application folder.
2. Make sure you have allowed screen recording for your browser in *Security & Privacy* Settings.

![image](assets/screen-recording-permission.png)

## M1 Mac

Follow instructions [here](./resources/macos/m1-game2text.md).

## :material-linux: Linux

Download the source code on [Game2Text's Repository](https://github.com/mathewthe2/Game2Text) and build it from source.

You would also need to install [Tesseract](https://tesseract-ocr.github.io/tessdoc/Home.html) for Linux.