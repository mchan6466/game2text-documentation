## Actually gamifying language learning

Game2Text is an open source tool for learning languages through games. It simplifies text extraction, definition lookup, and flashcard creation.

![image](https://user-images.githubusercontent.com/13146030/117099796-b3efa180-ada4-11eb-8c68-431dfa0acdb5.png)

If you are already playing JRPGs, visual novels, or basically anything in Japanese, you can turn your gaming session into opporunities to learn Japanese through the power of immersive learning.

## Complete toolbox at your fingertips

* **Popup Dictionary** - Hover over words for meaning
* **Kanji Finder** - Extract Japanese text from games through image recognition
* **Visual Novel Hooker** - Extract text from visual novels with text hooks
* **Game Script Matcher** - Use OCR to assist extracting dialog from game scripts
* **Translation** - Harness the power of DeepL, Google, and Papago translation services
* **Dictionaries** - Select a Japanese to English dictionary or a Japanese to Japanese dictionary
* **AnkiConnect** - Make flashcards in seconds, complete with screenshots and game audio

## Made for everyone

Game2Text is simple and easy to use. There is no need to write bash scripts or be an Anki wizard. Just play and learn.

Having trouble? Read our [Quick Start Guide](../user-guide/quick-start).
 
## Support

If you have any questions, email me at mchan6466@gmail.com
or feel free to ask on our [Discord](https://discord.gg/wDePbmHxE2).

<a href="https://discord.gg/wDePbmHxE2"><img src="../assets/discord-logo.svg" width="500"></a>