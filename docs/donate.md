## Hi! I'm Mathew Chan, the creator of Game2Text. Thank you for following this link!

I started Game2Text as a simple web app for my friend so she can simply look up words while playing on her Macbook, removing all the hassle of installing OCR apps and browser extensions.

Since then, I've converted it to a desktop app with everything from creating Anki cards on the fly to an embedding a Visual Novel hooker.

If you enjoyed the app and would like to support its development, you can make a one time donation through Kofi.

[![Kofi](../assets/kofi.png)](https://ko-fi.com/mathewchan)

Make a one-time contribution through Kofi!