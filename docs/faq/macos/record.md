

## Allow microphone access in security settings
Check that you have allowed microphone for the **Terminal** application in  **Security & Privacy** settings.

<img width="580" alt="Screenshot]¥" src="https://user-images.githubusercontent.com/13146030/116784216-a3e67200-aac5-11eb-9a16-96462cbc3b75.png">

## Install Blackhole 16ch
Install [Blackhole](https://github.com/ExistentialAudio/BlackHole), a virtual audio cable to pass system sound as a microphone.

If you have [brew](https://brew.sh/) installed, you can just type the following command to *Terminal*.

```bash
brew install blackhole-16c
```

## Add Multi-Output Device and set Blackhole as input

On your desktop type *command-space* to launch **Spotlight** and type in *midi* and press enter to launch **Auto MIDI Setup**.

In Auto MIDI Setup, click on the bottom left corner **+** icon and add a new **Multi-Output Device**. In the list of devices, check **Blackhole 16ch** and also check your speakers or headphone. 

<img width="580" alt="Screenshot 2021-05-01 at 9 42 03 PM" src="https://user-images.githubusercontent.com/13146030/116784464-e492bb00-aac6-11eb-94a2-deb13f079dde.png">

Type *command-space* to launch **Spotlight** again and type in *sound* to launch **Sound Settings**.

On the Output tab, select **Multi-Output Device** and on the Input tab select **BlackHole 16ch**.

<img width="580" alt="Screenshot 2021-05-01 at 9 45 49 PM" src="https://user-images.githubusercontent.com/13146030/116784426-b2815900-aac6-11eb-8a32-7f3ddeb29ff4.png">

<img width="580" alt="Screenshot 2021-05-01 at 9 52 15 PM" src="https://user-images.githubusercontent.com/13146030/116784568-87e3d000-aac7-11eb-880f-95900e259e6f.png">

## Set Blackhole as your audio device

In Game2Text, click on the cogwheel icon to launch the **Settings Dialog**.

Click on Audio Device and select Blackhole 16ch.

<img width="580" alt="Screenshot 2021-05-01 at 9 50 10 PM" src="https://user-images.githubusercontent.com/13146030/116784528-405d4400-aac7-11eb-86a3-69d89e98e330.png">

## Notes

On Mac OS, automatic recording is disabled when you launch Game2Text but can be re-enabled afterwards.