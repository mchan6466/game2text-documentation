On MacOS, make sure you have allowed screen recording for your browser in **Security & Privacy** settings.

<img width="580" alt="Screenshot" src="https://user-images.githubusercontent.com/13146030/113811992-d7243280-979f-11eb-8bdf-bcea6bd4e9bd.png">