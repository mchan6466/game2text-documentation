If you're playing a full screen game with dual monitors, the game may be **blacked out** in Game2Text. An easy solution would be to use an external capturing program like **OBS**.

!!! Info
    Game2Text can float on the same screen as your full screen game with the help of external tools. [Check this guide to see how.](../../user-guide/play-in-full-screen)

## Install OBS

[:material-download: Download OBS](https://obsproject.com/){ .md-button }


## Add your game window 

Launch OBS and add a source by clicking **+** on the bottom and selecting **Window Capture**.

<img src="../../assets/obs-1.png" alt="obs-1" width="500">

Select the **Window** that shows your game.

<img src="../../assets/obs-2.png" alt="obs-2" width="500">

## Start a windowed projector

After confirming your game, you should be able to see the game in the center of OBS.

Right click on the game stream and select **Windowed Projector (Preview)**.

<img src="../../assets/obs-3.png" alt="obs-3" width="500">

A separate window will pop out showing only the video stream. There is no need to resize this window.

<img src="../../assets/obs-4.png" alt="obs-4" width="500">

## Select the windowed projector in Game2Text

Back in Game2Text, select the windowed projector window.

<img src="../../assets/obs-5.png" alt="obs-5" width="500">

The full-screen game will be shown in Game2Text and you can use the app as usual.

<img src="../../assets/obs-6.png" alt="obs-6" width="500">

<img src="../../assets/obs-7.png" alt="obs-7" width="500">