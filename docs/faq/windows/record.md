System audio is recorded on Windows through WASAPI and is supposed to work for all your audio devices. Windows booted through a VM or Bootcamp may not work, in that case follow this guide for setting up a Virtual Audio Cable.

## Install a Virtual Audio Cable (VAC)

[VB Audio](https://vb-audio.com/Cable/) is the most popular Virtual Audio Cable for Windows.

## Set the VAC as your system's recording device

On the the bottom right of your Windows task bar right click on **Soundtray** and select **Sound**.

Set the VAC as the **recording device** and click **Properties**. Select the **Listen** tab and enable **Listen to this device**

<img height="600" alt="vac_settings" src="https://user-images.githubusercontent.com/13146030/116955699-da8adb00-acc5-11eb-86db-733e97efebe0.png">

## Set the VAC as your audio device in Game2Text

Back in Game2Text, click **Settings** and select the **Media** tab. Select the VAC as your audio device.

<img width="590" alt="sound_settings" src="https://user-images.githubusercontent.com/13146030/116955941-8502fe00-acc6-11eb-906a-8ac593e1f7ab.png">