## Use hotkeys
The default hotkey for OCR refresh is **Control-Q** on Windows (**Command-B** on Mac). You can change the hotkey in :material-cog: **Settings** > **Hotkeys**.

<img src="../../assets/hotkeys.png" alt="hotkeys" width="500">

Please see [hotkeys](../user-guide/hotkeys.md) for more details.

## Enable Auto-mode
[Enable auto-mode](../user-guide/auto-mode.md) by pressing **:material-autorenew: Auto Mode** in the toolbar.

<img src="../../assets/auto-enabled.png" alt="auto-enabled" width="500">
