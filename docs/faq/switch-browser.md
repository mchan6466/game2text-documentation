Replace the browser value in *browser=[your browser]* in the *config.ini* file. 

- :material-google-chrome: *chrome*: Chrome
- :material-firefox: *chromium*: Firefox, Opera, Brave, and other chromium browsers installed.
- :material-microsoft-edge-legacy: *edge*: Microsoft Edge
- :material-google-chrome: *default*: (Windows Only) finds your default browser 

<img src="https://user-images.githubusercontent.com/13146030/113812636-02f3e800-97a1-11eb-8435-5f2c0e7b0339.png" width="300">