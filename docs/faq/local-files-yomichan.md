When you open text dumps and subtitle files like SRT on Chrome, Yomichan needs local file access to work.

Type *chrome://extensions/* in the browser and find Yomichan.

In the settings enable **Allow access to file URLS**.

![image](../../assets/yomichan-local-file-access.png)

Yomichan should work with your local files now.

![image](../../assets/yomichan-srt.png)