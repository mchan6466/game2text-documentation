## Anki

Anki is the most popular flashcard app for spaced repetition. It stores your word cards and also helps you retain your learned word by periodic revision.

[:material-download: Download Anki](https://apps.ankiweb.net/){ .md-button }

![image](https://miro.medium.com/max/1400/0*iaW-cWv3Fvs8UEcm.png)

## AnkiConnect

Game2Text, Yomichan, Animebook and other learning apps require the [AnkiConnect addon](https://ankiweb.net/shared/info/2055492159) to connect to Anki. 

To install AnkiConnect, launch Anki and go to **Tools > Add-ons > Get Add-ons**. Enter the code from the AnkiConnect website and Anki will install the add-on.

At the time of this writing, the code is **2055492159**.

![image](https://miro.medium.com/max/1400/0*dNTQ7wUhOTnnsrQn.png)

!!! info
    MacOS users may need to [follow the instructions here ](https://foosoft.net/projects/anki-connect/#notes-for-mac-os-x-users) and enter 3 system commands to their Terminal.

