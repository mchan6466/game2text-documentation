
## JRPG and VN mining

With Agent, we can extract text from popular JRPGs and visual novels and add them to Anki all on the Steam deck.

![Alt Text](../../assets/steam-deck-mining/handheld-mining-demo.gif)

This works for games such as Persona (3-5), Yakuza, Steins Gate, Danganronpa and more. You can find the full list [here](https://github.com/0xDC00/scripts).

## Getting Started

We'll use the following applications. Switch to Steam Deck's desktop mode for the following steps.

### Applications
- [Anki on Flathub](https://flathub.org/apps/net.ankiweb.Anki) 
- [AnkiConnect](https://ankiweb.net/shared/info/2055492159) Anki addon
- [Agent](https://github.com/0xDC00/agent/releases) Text Extractor (Windows version)
- [Steam Tinker Launch](https://github.com/sonic2kk/steamtinkerlaunch/wiki/Installation#steam-deck)
- [Chrome on Flathub](https://chromewebstore.google.com/detail/yomitan/likgccmbimhjbgkjambclfkhldnlhbnn)
- [Yomitan](https://chromewebstore.google.com/detail/yomitan/likgccmbimhjbgkjambclfkhldnlhbnn) Chrome extension
- [Clipboard Inserter](https://chromewebstore.google.com/detail/clipboard-inserter/deahejllghicakhplliloeheabddjajm?hl=en) Chrome extension

!!! info
To switch to desktop mode, press the bottom right `Steam` button, select `Power`, then `Switch to Desktop`. If `Switch to Desktop` is unavailable, look for `Exit Big Picture Mode`.

Even though the Steam Deck uses Linux, we are using the Windows version of Agent. It will run alongside our game through a compatibility layer provided by Steam Tinker Launch.

If you're having trouble installing the tools, you can follow along with this video tutorial.

## Steam Deck Mining Tutorial
<iframe width="560" height="315" src="https://www.youtube.com/embed/-RVR3agVFTk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Adding Agent to your game

Once you have everything installed, launch Steam, select Library, right click on your game and select Properties. Select `Compatibility`, enable `Force the use of a specific Steam Play compatibility tool` and select `Steam Tinker Launch` from the dropdown menu.

![image](../../assets/steam-deck-mining/force-compatibility.png)

Launch the game, and Steam Tinker Launch's menu will pop up. Select `Main Menu` on the bottom right, and `Game Menu` in the following popup. Under `Misc options`, enable `Use custom command` and select the `agent.exe` file inside the Agent application folder. Select `Save`. The next time the game is launched, `Agent` will be launched at the same time.

![image](../../assets/steam-deck-mining/steam-tinker-launch-menu.png)

![image](../../assets/steam-deck-mining/steam-tinker-launch-game-menu.png)

## Extracting text to Chrome

On Agent, hold down the target icon on the top right and drag it to the game window. This will fill in the game title for you. Type in the `Script` input and search for your game. If this is the first time you are using Agent, click on the button next to the Script input and select `Update scripts` to fetch the scripts online. Finally, select `Attach`.

![image](../../assets/steam-deck-mining/agent-attached.png)

On Chrome, pin the `Clipboard Inserter` extension and toggle to enable it. Go to this [Texthooker page](https://learnjapanese.moe/texthooker.html), and text from the game will be pasted on Chrome as you play.

![image](../../assets/steam-deck-mining/chrome-extracted-text.png)

For information on setting up Yomitan and Anki, visit [this site](https://learnjapanese.moe/yomichan/).

## Adding the window toggle script

Launch the pre-installed text editor `Kate`. Paste the following script and change the name of your game window. We only need to type in its prefix for our cli tool to locate the game window. For example, if your game window's name is `SG 線形拘束のフェノグラム`, you can just put in `SG`.

```shell
game_name=SG # change this line to the name of your game window

game_window=$(xdotool search -name --desktop 0 $game_name)
active_window=$(xdotool getactivewindow)
chrome_window=$(xdotool search -name --desktop 0 Chrome)
if [ $active_window -eq $chrome_window ]; then
  xdotool windowactivate $game_window
else
  xdotool windowactivate $chrome_window
fi
```

![image](../../assets/steam-deck-mining/kate-edit-script.png)

!!!info
To quickly open apps, use the shortcut alt+space or command+space, type in the name of the application, and press enter.

Click `Save As` and save the file as `toggle_window.sh` to your Desktop.

!!!info
Here's a quick way to test the script. While a Chrome window is open, launch `Konsole`, type in `sh Desktop/toggle_window.sh` and see if your Chrome window is brought to focus. If not, check to see if you accidentally removed xdotool or renamed Chrome.

## Adding a system shortcut to invoke the script

Click `System Settings` on the bottom left status bar. Under `Workspace`, select `Shortcuts` > `Custom Shortcuts`. On the bottom, select `Edit` > `New` > `Global Shortcut` > `Command/URL`. Name it as `Toggle browser`. 

![image](../../assets/steam-deck-mining/toggle-shortcut-trigger.png)

On the top right, select `Trigger` and click on the input box, and hit the `CapsLock` key on your keyboard. Select the `Action` tab and type in `sh /home/deck/Desktop/toggle_window.sh`. Finally, select `Apply` on the bottom right.

![image](../../assets/steam-deck-mining/toggle-shortcut-action.png)

!!!warning
Not every key or shortcut can be used during game mode. We recommend `CapsLock`, as it is rarely used.

## Mapping the Steam Deck controller to the system shortcut

We need to map the controller twice, one for Desktop, and one for our game.

### Mapping on desktop

On Steam, select `Steam` on top and select `Settings`. Select `Controller`, and under `Non-game Controller Layouts`, select `Edit` for the `Desktop Layout`. 

![image](../../assets/steam-deck-mining/steam-controller-settings.png)

Select `Edit Layout`, click :material-cog: for the X key, and select `Remove command`. Select `Add command`, `Keyboard`, and click on the `CapsLock` button on the keyboard layout. Click `Back` several times on the bottom right until the window closes.

![image](../../assets/steam-deck-mining/controller-settings-keyboard.png)

![image](../../assets/steam-deck-mining/controller-x-key-to-caps-lock.png)

### Mapping in-game

On Steam, select `Library`, right click on your game, and select `Properties`, Select `Controller`, and select `Controller Configurator`. 

Similarly, select `Edit Layout`, click :material-cog: for the X key, and select `Remove command`. Select `Add command`, `Keyboard`, and click on the `CapsLoc` button on the keyboard layout. 

!!!info
For visual novels like Steins Gate that do not support controllers on desktop mode, we could also map the A, B keys to `Enter` and `Right Mouse Click`. For `DPad`, we could map the directional keys to the Desktop arrow keys.

![image](../../assets/steam-deck-mining/map-directional-keys.png)

With Chrome and the game window open, push the X key on your Steam Deck to test if it successfully toggles focus bewteen your game and Chrome.

## Hiding bottom taskbar

You may want to hide the bottom taskbar while playing on desktop mode. Right click on the taskbar, enter edit mode, `More options`, and select `Windows Can Cover` or `Auto Hide`.

![image](../../assets/steam-deck-mining/hide-task-bar.png)