# How to use Yomichan with Amazon JP books

## Requirements

1. An Amazon JP account
2. A physical Kindle device
    - Amazon Kindle
    - Paperwhite
    - Oasis
    - Voyage
    - ...other physical Kindle readers.

## How to buy Amazon JP Books

#### Amazon JP account
To buy books on the Japanese Amazon site, you will need an Amazon account on amazon.co.jp. It should be a separate account and not the one you're using. As such, it is recommended that you use a different email address to register for your Japanese account.

You can switch to the English interface if you are not used to the Japanese version. 

The display language does not affect the book selection or recommendations on the site. 

<img src="../../assets/ln-mining/amazon-language.png" alt="amazon-language" width="400">

Check the address bar to make sure the web address still uses the amazon.co.jp domain in case you have been redirected to the global site.

#### Japanese address

A Japanese address is required for account registration. 

You will get an error if you fill in random numbers since it checks against real Japanese post codes.

You can use services like DANKEBOX to get a free Japanese address and P.O. box.

![image](../assets/ln-mining/dankebox.png)

The P.O. box can also come in handy if you plan on buying physical goods on Amazon or other websites like Rakuten and Mercari. 

You will have to pay for the shipping cost for DANKEBOX to mail you the items, but it's still great to have access to a much larger Japanese market, including new and second-hand items that do not offer worldwide shipping.

Other similar services: [Tenso](https://www.tenso.com/), [White Rabbit Express](https://www.whiterabbitexpress.com/)

#### Japanese VPN

A Japanese VPN is usually not required for shopping on amazon.co.jp, but some people have experienced regional issues. 

If you see the following message, simply changing your account's region will not resolve the problem.

![image](../assets/ln-mining/amazon-region.png)

In this case, consider purchasing a VPN service or hosting your own VPS using tools like [Outline VPN](https://getoutline.org/en-GB/).


#### Buy the Kindle version

To read with Yomichan, buy the **Kindle (Digital)** version of the book.

![image](../assets/ln-mining/amazon-buying-digital.jpg)

## Download the azw3 ebook 

Go to amazon.co.jp and head to **Your Account**.

Select **Content and Devices** in *Digital content and devices*.

![image](../assets/ln-mining/amazon-content-and-devices.png)

Locate your ebook and expand the action menu.

![image](../assets/ln-mining/amazon-books-action.png)

Select **Download & Transfer via USB**.

![image](../assets/ln-mining/amazon-book-download.png)

Select your Kindle device. This cannot be a phone or a computer.

<img src="../../assets/ln-mining/amazon-select-kindle.png" alt="amazon-language" width="400">

When the download is complete, drag the *azw3* file to Calibre.

## Convert to EPUB with Calibre

#### Add DeDRM to Calibre

Go to [DeDRM's releases](https://github.com/apprenticeharper/DeDRM_tools/releases) and download the latest version.

![image](../assets/ln-mining/dedrm.png)

Unzip the *DeDRM_tools.zip* file. *DeDRM_plugin.zip* will be in the folder. There is no need to further unzip this file.

In Calibre, go to Preferences > Plugins.

![image](../assets/ln-mining/calibre-preferences.png)

Select *Load plugin from file*.

![image](../assets/ln-mining/calibre-load-plugin.png)

Select **DeDRM_plugin.zip**.

![image](../assets/ln-mining/dedrm-plugin.png)

Once added, you can find the plugin here.

![image](../assets/ln-mining/calibre-dedrm.jpg)

Double click on the plugin to launch its settings.

Select **eInk Kindle ebooks**.

<img src="../../assets/ln-mining/dedrm-config.png" alt="dedrm-config" width="300">

Click on the *+* sign and add your Kindle Serial Number. 

To find your Kindle's serial number, tap **Settings > Device info**.

![image](../assets/ln-mining/kindle-serial-number.jpg)

You can find the serial number for your Kindle device by going into Settings.

<img src="../../assets/ln-mining/dedrm-kindle-serial-number.png" alt="dedrm-config" width="300">

Close the dialog and restart Calibre.

Once DeDRM has been added and configured, we can skip this section the next time we convert a book.

#### Convert book to EPUB format

In Calibre, right click the previously added ebook and select **Convert books > Convert individually**.

![image](../assets/ln-mining/calibre-convert-book.jpg)

In the top right corner, change the *Output format* to **EPUB**. 

Cick **Ok** in the bottom right corner to export.

![image](../assets/ln-mining/calibre-convert-to-epub.jpg)

Once export is complete, right click on the book in Calibre again and select **Open containing folder**.

![image](../assets/ln-mining/calibre-open-book-folder.jpg)

You will be able to move or copy your EPUB file from your folders.

![image](../assets/ln-mining/calibre-book-folder-contents.png)

## Read EPUB on Web Browser

Go to [TTU Reader](https://ttu-ebook.web.app/). 

![image](../assets/ln-mining/ttu-reader.png)


Now drag and drop the EPUB file to the browser.

![image](../assets/ln-mining/ttu-book-vertical.jpg)

You can now use [Yomichan](https://chrome.google.com/webstore/detail/yomichan/ogmnaimimemjmbakcfefmnahgdfhfami) or other popup dictionaries on the book.

![image](../assets/ln-mining/ttu-vertical-yomichan.jpg)

Horizontal text orientation is recommended and could be selected in the display settings.

## Enhance LN mined cards

After configuring Yomichan's Anki settings and installing AnkiConnect, you can add cards to Anki with a click of a button

![image](../assets/ln-mining/ttu-yomichan-anki.jpg)

Yomichan can map useful information like the English meaning, Japanese definition, pitch accent, word type. 

This is what mine looks like. 

![image](../assets/ln-mining/ln-anki-card.png)

We can build extra context for these cards by adding example sentences from [Immersion Kit](https://www.immersionkit.com).

Simply search for a word on the website and find a sentence you like. 

![image](../assets/ln-mining/immersion-kit-search.png)

In the results, find an example you like. Toggle the **Mining** section at the bottom of the example. 

Click on any one of the fields to copy.

![image](../assets/ln-mining/immersion-kit-mining.jpg)

In Anki, paste the content to your fields with Ctrl-V (Win) or Cmd-V (Mac).

![image](../assets/ln-mining/ln-card-with-context.jpg)

## Progress Tracking

Unfortunately, TTU reader is not great for progress tracking and bookmarking.

Use Calibre to save your progress or online websites such as [Book Meter](https://bookmeter.com) and [Learn Natively](https://learnnatively.com/).

## Android E-ink Device 

You can also mine vocabulary with an Android e-ink device.

For reference, I am using a Boox Nova Pro running Android 6.0.1.

<img src="../../assets/ln-mining/eink-yomichan.jpg" alt="eink-yomichan" width="300">

### Install Kiwi Browser and AnkiDroid

Get [Kiwi Browser](https://play.google.com/store/apps/details?id=com.kiwibrowser.browser) and [AnkiDroid](https://play.google.com/store/apps/details?id=com.ichi2.anki&hl=ja&gl=US) from the Google Play Store.

Login to your Anki account on AnkiDroid.

<img src="../../assets/ln-mining/ankidroid.png" alt="ankidroid" width="300">

### Install Yomichan on Kiwi Browser

Get Yomichan directly from the Chrome extension site while using the Kiwi browser as you would usually in Chrome.

Download the JMDict dictionary and import to Yomichan.

<img src="../../assets/ln-mining/kiwi-yomichan.png" alt="ankidroid" width="300">

### Install AnkiConnect Android

Download [AnkiConnect Android](https://github.com/KamWithK/AnkiconnectAndroid) and transfer it via USB to your device. On Mac, you can do this with [Android File Transfer](https://www.android.com/filetransfer/).

!!! Info
    When you install a third party app for the first time, you will be given a prompt to allow third party apps. You may also need to disable *Google Play Protect* on the Play Store.

Launch the app and tap **Start Service**.

<img src="../../assets/ln-mining/ankiconnect-android.png" alt="eink-yomichan" width="300">

!!! Warning
    If your Android version is under 8.0, use [this compatiblity build](https://github.com/KamWithK/AnkiconnectAndroid/files/9296028/ankiconnect_android_1_5_compatibility.apk.zip).

### Set up Yomichan

Set scan to “no key”, delay to 0, and enable Anki.

!!! info
    To see further settings in Yomichan, select top right :material-dots-vertical: > Extensions > Yomichan (Details) > Extension options.

It is recommended to add more audio sources. In audio settings, select *Configure audio playback sources…* and add *JapanesePod101 (Alternate)* as well as *Custom URL(JSON)*. Type in *http://localhost:8770/?expression={term}&reading={reading}* for the URL to enable Forvo audio.

### Transfer EPUB book

Transfer your EPUB book to the e-reader via USB. Refer to the aforementioned instructions on how to convert your Amazon book to an EPUB file. 

In the Kiwi browser visit [TTU Reader](https://ttu-ebook.web.app/) and select the EPUB file. Everything should work now. Enjoy mining!

<img src="../../assets/ln-mining/kiwi-ttu.png" alt="ankidroid" width="300">

To read in full screen, tap right below the browser toolbar to review ttu's menu. Click on the expanding icon on the top right. 

<img src="../../assets/ln-mining/kiwi-ttu-toolbar.png" alt="ankidroid" width="300">

Tap on :material-cog: settings on top right, and enable **Auto Bookmark**. This will save your current page when you switch apps or sleep the device.

<img src="../../assets/ln-mining/ttu-auto-bookmark.png" alt="ankidroid" width="300">

If you have any issues, please feel free to ask on our [discord](https://discord.gg/wDePbmHxE2).

