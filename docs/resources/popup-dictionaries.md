## Yomichan

Popup dictionaries let you quickly look up words and phrases on your browser as well as in Game2Text.

[:material-download: Download Yomichan ](https://chrome.google.com/webstore/detail/yomichan/ogmnaimimemjmbakcfefmnahgdfhfami){ .md-button }

![image](https://miro.medium.com/max/1280/1*hG9hlhyc5_RFK7IH5w9G3Q.jpeg)

[Learn how to set up Yomichan](https://yui-spl2.medium.com/forgot-a-japanese-word-you-saw-try-this-chrome-extension-86bfdb7481dd)

## Recommended Yomichan Dictionary: Jisho+

<img src="../../assets/jmdict+.png" alt="jmdict-plus" width="500">

Jisho+ is a tagged JMDict dictionary to show the JLPT level and WK level of the words in the dictionary. 

[:material-download: Download Jisho+ ](https://community.wanikani.com/t/yomichan-and-wanikanijlpt-tags/37535/14){ .md-button }

