## What It Does
This script enables you to to record audio and have the result in your clipboard so you can directly paste it to Anki or another folder.

This is really helpful when creating Anki cards with audio data. 

## Create the Application

Launch **Automator** by pressing Command⌘+Space for *Spotlight*, typing in *automator*, and hitting *Enter*.

![image](../../assets/automator-spotlight.png)

Create an **Application**.

![image](../../assets/automator-application.png)

Add a **Run AppleScript** action by searching for *applescript* in the upper left search bar and double clicking the *Run AppleScript* item.

Paste the following script to the action.

```applescript
set appName to "VoiceMemos"
if application appName is running then
	tell application id (id of application appName)
		quit
	end tell
	copy {do shell script "echo $HOME"} to {homeFolder}
	set recordingsFolder to homeFolder & "/Library/Application Support/com.apple.voicememos/Recordings"
	set audioFileName to (do shell script "cd " & quoted form of recordingsFolder & " && ls -ltr -A1 | grep m4a | tail -1")
	set the clipboard to POSIX file (recordingsFolder & "/" & audioFileName)
else
	tell application id (id of application appName) to activate
	tell application "System Events"
		set limit to 10
		set counter to 0
		repeat
			if exists window 1 of process appName then exit repeat
			if (counter > limit) then exit repeat
			set counter to counter + 1
			delay 1
		end repeat
		perform action "AXRaise" of window 1 of process appName
		keystroke "n" using command down
	end tell
end if
```

Press **Command+S** and name the application as *Audio To Clipboard*.

![image](../../assets/automator-audio-to-clipboard.png)

## Create the Quick Action

While in **Automator**, click *File > New* and create a new **Quick Action**.

![image](../../assets/automator-quick-action.png)

Add **Launch Application** and select the newly created Audio To Clipboard.app located in ~/Library/Services folder.

Hit **Command+S** and name it as *Voice To Clipboard*.

![image](../../assets/automator-voice-to-clipboard.png)

!!! info
    Type in the file name in the top right Search bar if you’re having trouble locating the application

![image](../../assets/macos-services-location.png)

Then go to System Preferences > Keyboard > Shortcuts. Select Services from the sidebar and find the service Voice To Clipboard. Add a shortcut by double clicking (none).

I use the hotkey **Command+Control+V (^⌘V)**. 

![image](../../assets/macos-keyboard-services-voice-to-clipboard.png)

!!! info 
    Command+Control+A-Z are usually available for your custom global shortcuts, with Command+Control+F being a notable exception.

When you run the script for the first time through the keyboard shortcut, you may need to give **Accessibility** permission to *Audio To Clipboard*.

![image](../../assets/macos-accessibility-audio-to-clipboard.png)

Once permission is given, you can type in the shortcut (^⌘V) anywhere on your Mac to record audio and press Command+V to directly paste it to the Anki Audio field.

![image](../../assets/macos-anki-paste-audio.png)

To record sound going through your browser on Netflix or in a game, you would also need to set up a Virtual Audio Cable.

## Recording System Audio through BlackHole

Install [Blackhole](https://github.com/ExistentialAudio/BlackHole), a virtual audio cable to pass system sound as a microphone.

If you have [brew](https://brew.sh/) installed, you can just type the following command to *Terminal*.

```bash
brew install blackhole-16c
```

## Add Multi-Output Device and set Blackhole as input

On your desktop type *command+space* to launch **Spotlight** and type in *midi* and press enter to launch **Auto MIDI Setup**.

In Auto MIDI Setup, click on the bottom left corner **+** icon and add a new **Multi-Output Device**. In the list of devices, check **Blackhole 16ch** and also check your speakers or headphone. 

<img width="580" alt="Screenshot 2021-05-01 at 9 42 03 PM" src="https://user-images.githubusercontent.com/13146030/116784464-e492bb00-aac6-11eb-94a2-deb13f079dde.png">

Type *command-space* to launch **Spotlight** again and type in *sound* to launch **Sound Settings**.

On the Output tab, select **Multi-Output Device** and on the Input tab select **BlackHole 16ch**.

<img width="580" alt="Screenshot 2021-05-01 at 9 45 49 PM" src="https://user-images.githubusercontent.com/13146030/116784426-b2815900-aac6-11eb-8a32-7f3ddeb29ff4.png">

<img width="580" alt="Screenshot 2021-05-01 at 9 52 15 PM" src="https://user-images.githubusercontent.com/13146030/116784568-87e3d000-aac7-11eb-880f-95900e259e6f.png">

System sound coming from your browser or your game will now become the default microphone source.