## What It Does
This script enables you to extract text from visual media like games, manga, videos and copies the result to your clipboard.

![image](../../assets/macos-ocr-clipboard.gif)

You can then quickly look the words up through spotlight or through Yomichan by using use a clipboard inserter, or apps like Game2Text to facilitate your flashcard creation workflow.

## Install Tesseract
Install [Tesseract](https://github.com/tesseract-ocr/tesseract), the OCR engine.

If you have [brew](https://brew.sh/) installed, you can just type the following commands to *Terminal*.

```bash
brew install tesseract
brew install tesseract-lang
```

## Create the Quick Action

Launch **Automator** by pressing Command⌘+Space for *Spotlight*, typing in *automator*, and hitting *Enter*.

![image](../../assets/automator-spotlight.png)

Create a **Quick Action**.

![image](../../assets/automator-quick-action.png)

Add a **Run AppleScript** action by searching for *applescript* in the upper left search bar and double clicking the *Run AppleScript* item.

Paste the following script to the action.

=== "macOS Montery"

    ```applescript
    set outPath to "/tmp"
    set tesseractCmd to (do shell script "zsh -l -c 'which tesseract'")
    do shell script "screencapture -i " & outPath & "/untitled.png"
    do shell script tesseractCmd & " " & outPath & "/untitled.png " & outPath & "/output -l jpn"
    set the_text to (do shell script "cat " & outPath & "/output.txt")
    set the clipboard to the_text
    do shell script "rm " & outPath & "/untitled.png " & outPath & "/output.txt"
    ```

=== "macOS Big Sur or lower"

    ```applescript
    set outPath to "/tmp"
    set tesseractCmd to (do shell script "bash -l -c 'which tesseract'")
    do shell script "screencapture -i " & outPath & "/untitled.png"
    do shell script tesseractCmd & " " & outPath & "/untitled.png " & outPath & "/output -l jpn"
    set the_text to (do shell script "cat " & outPath & "/output.txt")
    set the clipboard to the_text
    do shell script "rm " & outPath & "/untitled.png " & outPath & "/output.txt"
    ```

!!! info
    When you run the script, you might need to go to System Preferences > Security & Privacy > Screen Recording and give Automator permission to read the contents of the screen.

Press **Command⌘+S** and name the action as *Capture2Clipboard*.

![image](../../assets/automator-capture2clipboard.png)

**Run** the action, draw a selection around some text on your screen and paste the result to a text editor to see if works.

## Add a Keyboard Shortcut

Go to System Preferences > Keyboard > Shortcuts. Select Services from the sidebar and find the service *Capture2Clipboard*. Add a shortcut by double clicking (none).

![image](../../assets/macos-keyboard-service-capture2clipboard.png)

I use the hotkey **Command+Control+O (^⌘O)**. 

!!! info 
    Command+Control+A-Z are usually available for your custom global shortcuts, with Command+Control+F being a notable exception.

![image](../../assets/macos-keyboard-service-capture2clipboard-shortcut.png)

!!! info
    You might also need to go to System Preferences > Security > Privacy > Accessibility and give Automator permission to read keystrokes.

Whenever you press the hotkey, you can select a region to extract text and the result will go to your clipboard.