## Look up a word in Spotlight

Press Command⌘+Space to launch spotlight and enter the word to look up. You can immediately get the Japanese or English definition as well as example sentences.

![image](../../assets/spotlight-dictionary.png)

To activate Japanese dictionaries on your Mac, launch the Dictionary app (by typing command+space for Spotlight and typing in Dictionary) and on the top left corner click Dictionary > Preferences.

<div style="text-align:center"><img src="../../../assets/macos-dictionary-preferences.png" alt="dictionary-preferences" width="500"></div>

!!! info
    By default you get the J-E dictionary The *Wisdom English-Japanese Dictionary* and the J-J dictionary *スーパー大辞林*.

You can reorder these dictionaries to either show the Japanese definition or English definition as the first result. 

## MacOS Popup Dictionary

MacOS comes preinstalled with a popup dictionary that can be accessed with force touch, three finger touch, or right click > *Look up word*.

Unlike browser popup dictionaries like Yomichan and Rikakun that only works in browsers, **this popup dictionary works on all native apps and almost every desktop app**.

![image](../../assets/macos-popup-dictionary.png)

!!! info
    You might need to activate this feature in System Settings > Touchpad

## Add Jisho, Weblio and other online dictionaries

You can easily add Jisho, Weblio, or other online dictionaries as a *Service*.

![image](../../assets/macos-services-jisho.png)

Launch **Automator** by pressing Command⌘+Space for *Spotlight*, typing in *automator*, and hitting *Enter*.

![image](../../assets/automator-spotlight.png)

Create a **Quick Action**.

![image](../../assets/automator-quick-action.png)

For **Workflow** select *receives current text*.

![image](../../assets/automator-workflow-text.png)

Add a **Run JavaScript** action by searching for *javascript* in the upper left search bar and double clicking the *Run JavaScript* item.

![image](../../assets/automator-run-javascript.png)

Replace the line “ // Your script goes here” with this line of code

```javascript
input[0] = 'https://jisho.org/search/' + input[0];
```

Or use this line of code for Weblio:

```javascript
input[0] = 'https://ejje.weblio.jp/content/' + input[0];
```

Next, search and add a **Website Pop-up** action to the workflow.

![image](../../assets/automator-jisho.png)

I set the *Site Size* to **iPhone** so the popup window doesn’t take over my screen.

Press **Command⌘+ S** and save the quick action as *Jisho*.

Now back in your browser or any other app, select a word and right click **Services > Jisho**.

![image](../../assets/macos-services-dictionary.png)

![image](../../assets/macos-services-weblio.png)

Close the window by pressing the **Esc** key.

## Text to Speech in Japanese

To enable text to speech in Japanese, you have to configure a Japanese system voice.

In System Preferences > Accessibility > Speech, click **System Voice** and select **Customize**.

![image](../../assets/macos-text-to-speech.png)

Scroll down to select **Japanese (Japan)** voices. It might take a few minutes to install these voices. After hitting **OK**, switch the system voice to the newly installed voice.

![image](../../assets/macos-system-voice.png)

Whenever you right click on a text selection, select Speech > Start Speaking to hear the word being said.

!!! info
    The voice in text-to-speech is synthetic. If you prefer natural voice, use online services like Jisho or Forvo.

![image](../../assets/macos-speech-start-speaking.png)