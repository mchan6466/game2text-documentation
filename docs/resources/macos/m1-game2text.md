## Download

Download Game2Text on Github

https://github.com/mathewthe2/Game2Text

<img src="../../../assets/download-source.png" alt="download-source" width="500">

Select *Download ZIP*, then double click the zip fille to extract. 

## Install Tesseract

Type the following commands to *Terminal*.

```bash
brew install tesseract
brew install tesseract-lang
```

## Install PortAudio

Type the following commands to *Terminal*.

```bash
brew install portaudio
pip3 install --global-option='build_ext' --global-option='-I/opt/homebrew/include' --global-option='-L/opt/homebrew/lib' pyaudio
```

## Install Python

Type the following commands to *Terminal*.

```bash
brew install python@3.8
```

## Create virtualenv in Game2Text Folder

Install virtualenv. 

```bash
pip3 install virtualenv
```

Navigate to Game2Text folder.

```bash
cd ~/downloads
cd Game2Text-main
```

Set up python environment.

```bash
virtualenv venv --python=python3.8
source venv/bin/activate
```

## Install Python Packages

```bash
pip install eel pytesseract psutil opencv-python pydub fuzzywuzzy requests googletrans parse pynput pyperclip pyyaml sudachipy 
pip install sudachidict_small
```

## Update Tesseract Path

Locate your tesseract by entering `which tesseract` in terminal. Copy the path.

```bash
which tesseract
$ /opt/homebrew/bin/tesseract
```
Modify *tools.py* (line 32) to match your tesseract path.

<img src="../../../assets/m1-mac-tesseract-path.png" alt="m1-mac-tesseract-path" width="500">

## Running Game2Text

```bash
python game2text.py
```