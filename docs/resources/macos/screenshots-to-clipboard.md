## Screenshot Shortcuts

You may already know these two screenshot shortcuts.

Action | Shortcut | 
--- | --- | 
Screen Capture Selection | Shift ⇧ + Command ⌘ + 4| 
Screen Capture Application | Shift ⇧ + Command ⌘ + 4 + Space | 

## Screenshot to Clipboard 

By pressing **Ctrl** at the same time while pressing the shortcuts, the image will be copied to your clipboard instead of being saved to your desktop.

Then you can directly paste it to an image field in Anki or to another folder in Finder.

![image](../../assets/anki-paste-screenshot.png)

## Remove default shadows

You may also find it handy to **remove the default shadows** applied to the screen capture image by holding **option** after you press the shortcut but before you click on the application window.